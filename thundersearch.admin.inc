<?php

/**
 * @file
 * Includes admin functions for thundersearch.
 */

/**
 * Implements hook_admin_settings().
 */
function thundersearch_admin_settings($form, &$form_state) {

  // PR value form.
  $form['thundersearch_pr'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Index Name, which is the PR value in URL string for Thundersearch'),
    '#default_value' => variable_get('thundersearch_pr'),
    '#description' => t('Enter the Thunderstone appliance search index name here. IMPORTANT: Upper/lower case is significant.'),
    '#required' => TRUE,
  );

  // Server URL form.
  $form['thundersearch_server_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of Thunderstone server'),
    '#default_value' => variable_get('thundersearch_server_url'),
    '#description' => t('Enter the URL of your Thunderstone search engine.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
