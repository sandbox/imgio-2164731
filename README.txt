CONTENTS OF THIS FILE
---------------------

 * Overview
 * Requirements
 * Installation / Configuration

Overview
--------

The thundersearch module interfaces the 
Thunderstone search engine to search on Drupal 
sites. It also comes with a world map to search 
by certain countries.


Requirements
------------
  - Drupal 7.x
  - PHP 5.2 or higher


Installation / Configuration
----------------------------

1. Simply install the module folder into the default 
module directory (sites/all/modules) like other modules
2. The configuration page for the module can be found 
at "admin/config/search/thundersearch"
