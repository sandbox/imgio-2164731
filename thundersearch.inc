<?php

/**
 * @file
 * Includes additional necessary functions for thundersearch to work.
 */

/**
 * Main form that creates the search field.
 */
function thundersearch_form($form, &$form_state, $query = '') {
  $urls = thundersearch_getUrls();

  $form = array(
    '#action' => $urls['result_url'],
    '#attributes' => array('class' => 'search-form'),
  );

  drupal_add_js("
   function register_click(obj){
   obj.form.query.value=obj.value;
   }
   ", 'inline');

  $options = array("" => "-Suggested Searches");
  $vocabulary = taxonomy_vocabulary_machine_name_load('thundersearch_terms');
  $result = db_query('SELECT t.tid, t.name, t.description FROM {taxonomy_term_data} t INNER JOIN {taxonomy_term_hierarchy} th ON th.tid = t.tid WHERE t.vid = :vid AND th.parent = 0 order by t.weight ', array(':vid' => $vocabulary->vid))->fetchAllAssoc('tid');

  foreach ($result as $terms) {
    $options[$terms->name] = $terms->description;
  }

  $form['basic']['inline'] = array('#prefix' => '<div class="container-inline">', '#suffix' => '</div>');
  $form['basic']['inline']['query'] = array(
    '#type' => 'textfield',
    '#title' => 'Search',
    '#default_value' => $query,
    '#size' => 20,
    '#maxlength' => 255,
  );
  $form['basic']['inline']['seldd'] = array(
    '#type' => 'select',
    '#title' => '',
    '#default_value' => "",
    '#attributes' => array('onchange' => 'register_click(this)', 'style' => 'width:250px;'),
    '#options' => $options,
  );
  $form['basic']['inline']['orderby'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => '',
    '#default_value' => ((!isset($_SESSION['sorderby'])) ? 'r' : $_SESSION['sorderby']),
    '#options' => array(
      '' => t('Sort results by'),
      'r' => t('Relevance'),
      'dd' => t('Date')),
    '#attributes' => array('onchange' => 'this.form.submit();'),
  );

  $form['basic']['inline']['submit'] = array('#type' => 'submit', '#value' => t('Search'));
  $form['#submit'][] = 'thundersearch_submit_page';

  return $form;
}

/**
 * Submits page.
 */
function thundersearch_submit_page($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['query']) && ($values['query'] != "")) {
    $_SESSION['squery'] = $values['query'];
  }
  if (isset($values['orderby']) && ($values['orderby'] != "")) {
    $_SESSION['sorderby'] = $values['orderby'];
  }
}

/**
 * Returns the results page.
 */
function thundersearch_result() {
  global $base_url;
  thundersearch_getKeyword();
  $keyword = ($_SESSION['squery'] != '') ? str_replace(" ", "+", $_SESSION['squery']) : "Human+Rights";
  $jump = thundersearch_getJump();
  $pr = thundersearch_getPR();
  $orderby = thundersearch_getOrderBy();
  $output = drupal_get_form('thundersearch_form', $_SESSION['squery']);

  $thundersearch_server_url = variable_get('thundersearch_server_url');

  $url = $thundersearch_server_url . "/texis/search/main.html?pr=" . $pr . "&amp;query=" . $keyword . "&amp;jump=" . $jump . "&amp;order=" . $orderby . "&amp;" . time();
  $results = '';
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

  if (($head = curl_exec($ch)) === FALSE) {
    $head = 'Curl error: ' . curl_error($ch);
  }

  curl_close($ch);

  $results = str_replace('/texis/search/main.html', $base_url . '/thundersearch/result', $head);
  $results = str_replace('/texis/search/', $base_url . '/thundersearch/result', $results);

  $output = drupal_render($output);
  $output .= $results;

  return $output;
}


/**
 * Function thundersearch_parseKeyWord().
 */
function thundersearch_parseKeyWord($keyword) {
  return (isset($keyword) && ($keyword != "")) ? str_replace("+", " ", $keyword) : "";
}

/**
 * Function thundersearch_getKeyword().
 */
function thundersearch_getKeyword() {
  if (isset($_REQUEST['query']) && ($_REQUEST['query'] != "")) {
    $_SESSION['squery'] = $_REQUEST['query'];
  }
  return (isset($_REQUEST['query']) && ($_REQUEST['query'] != "")) ? str_replace(" ", "+", $_REQUEST['query']) : "";
}

/**
 * Function thundersearch_getOrderBy().
 */
function thundersearch_getOrderBy() {
  if (isset($_SESSION['sorderby'])) {
    return $_SESSION['sorderby'];
  }
}

/**
 * Function thundersearch_getPR().
 */
function thundersearch_getPR() {
  return variable_get('thundersearch_pr');
}

/**
 * Function thundersearch_getJump().
 */
function thundersearch_getJump() {
  return (isset($_REQUEST['jump'])) ? $_REQUEST['jump'] : "0";
}

/**
 * Function thundersearch_getUrls().
 */
function thundersearch_getUrls() {
  global $base_url;
  $url = $base_url;
  $img_path = $url . '/' . drupal_get_path('module', 'thundersearch');
  $result_url = $url . "/thundersearch/result";
  return compact('url', 'img_path', 'result_url');
}

/**
 * Function thundersearch_parseHTML().
 */
function thundersearch_parseHTML($html) {
  $dom = new DOMDocument('1.0');
  @$dom->loadHTML($html);
  $dom->preserveWhiteSpace = TRUE;
  $dom->formatOutput = TRUE;

  $itr = 0;
  $paging_data = $page_data = $content = '';

  foreach ($dom->getElementsByTagName('table') as $data) {
    if ($itr == 0) {
      $paging_data .= thundersearch_construct_pagignation_table(thundersearch_getArray($data));
    }
    elseif ($itr == 1) {
      $tdata = thundersearch_getArray($data);
      if ($tdata['table']['tr']['td']['font']['0'] == 'No documents match the query.') {
        return 'No documents match the query.';
      }
      $page_data .= thundersearch_construct_data($tdata);
    }
    elseif ($itr == 3) {
      $content = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr><td align="center">';
      $content .= $paging_data . $page_data . $paging_data;
      $content .= '</td></tr></table>';

      return $content;
    }
    $itr++;
  }
}

/**
 * Function thundersearch_construct_data().
 */
function thundersearch_construct_data($tdata) {
  $tdata = $tdata['table']['tr']['td'][0]['table'];
  $data_tbl = '<table width = "' . $tdata[0]['width'] . '" border = "' . $tdata[0]['border'] . '" cellpadding = "' . $tdata[0]['cellpadding'] . '" cellspacing = "' . $tdata[0]['cellspacing'] . '" bgcolor = "' . ((isset($tdata[0]['bgcolor'])) ? $tdata[0]['bgcolor'] : "") . '">';
  if (is_array($tdata)) {
    foreach ($tdata as $data) {

      if (!(isset($data['tr']['td'][1]['strong']) || isset($data['tr']['td'][1]['div']['strong']))) {
        continue;
      }
      if (($data['tr']['td'][1]['strong']['a']['resulttitle'][0] || $data['tr']['td'][1]['div']['strong']['a']['resulttitle'][0]) == '') {
        continue;
      }

      $sdata = $data['tr']['td'];
      $font = (isset($sdata[0]['font'])) ? $sdata[0]['font'] : $sdata[0]['span'];

      $data_tbl .= '<tr><TD VALIGN="TOP" style="vertical-align: top;"><STRONG>' . $font['strong'] . '</STRONG></TD>';

      $font = (isset($sdata[1]['font'])) ? $sdata[1]['font'] : $sdata[1]['div']['font'];
      $adata = (isset($sdata[1]['strong']['a'])) ? $sdata[1]['strong']['a'] : $sdata[1]['div']['strong']['a'];
      $abstract = (isset($sdata[1]['abstract'])) ? $sdata[1]['abstract'] : $sdata[1]['div']['abstract'];
      $abs_font = (isset($sdata[1]['font']['strong']['urldisplay'])) ? $sdata[1]['font']['strong']['urldisplay'] : $sdata[1]['div']['font']['strong']['urldisplay'];
      if (!isset($abs_font)) {
        $abs_font = (isset($sdata[1]['font']['urldisplay'])) ? $sdata[1]['font']['urldisplay'] : $sdata[1]['div']['font']['urldisplay'];
      }

      $data_tbl .= '<TD VALIGN="TOP" WIDTH="99%" CLASS="content"><STRONG><A HREF="' . $adata['href'] . '" class="resultsLink" target="_blank">
      <ResultTitle xmlns:u="' . ((isset($adata['resulttitle']['xmlns:u'])) ? $adata['resulttitle']['xmlns:u'] : "") . '">' . $adata['resulttitle'][0] . '</ResultTitle></A></STRONG><br />
      <Abstract xmlns:u="' . ((isset($abs_font['xmlns:u'])) ? $abs_font['xmlns:u'] : "") . '">' . $abstract[0] . '</Abstract> ...<br />
      <FONT COLOR="FFFFFF" SIZE="-1"><strong><UrlDisplay xmlns:u="' . $abs_font['xmlns:u'] . '">' . $abs_font[0] . '</UrlDisplay></strong></FONT>';
      $data_tbl .= '</TD></tr>';
    }
  }
  $data_tbl .= '</table>';
  return $data_tbl;
}

/**
 * Function thundersearch_construct_pagignation_table().
 */
function thundersearch_construct_pagignation_table($tdata) {
  $urls = thundersearch_getUrls();
  $pagign_tbl = '<table width = "' . $tdata['table']['width'] . '" border = "' . $tdata['table']['border'] . '" cellpadding = "' . $tdata['table']['cellpadding'] . '" cellspacing = "' . $tdata['table']['cellspacing'] . '" bgcolor = "' . $tdata['table']['bgcolor'] . '"><tr>';

  if (isset($tdata['table']['tr']['td'][0]['a'])) {
    $pagign_tbl .= '<TD WIDTH="100" ALIGN="LEFT"><a href="' . thundersearch_parseURL($tdata['table']['tr']['td'][0]['a']['href']) . '"><IMG SRC="' . $urls['img_path'] . '/previous.gif" HSPACE="0" VSPACE="0" BORDER="0"></a></TD>';
  }
  else {
    $pagign_tbl .= '<TD WIDTH="100" ALIGN="LEFT"></TD>';
  }

  $pagign_tbl .= '<td align="center" style="text-align:center;">' . $tdata['table']['tr']['td'][1]['#text'] . '<br /> Page: ';
  $adata = $tdata['table']['tr']['td'][1]['a'];
  $bdata = $tdata['table']['tr']['td'][1]['b'];

  if (is_array($adata)) {
    foreach ($adata as $data) {
      if (($data[0] - 1) == $bdata) {
        $pagign_tbl .= '<b>' . $bdata . '</b> ';
      }
      $pagign_tbl .= '<a href="' . thundersearch_parseURL($data['href']) . '" class="' . $data['class'] . '">' . $data['0'] . '</a>&nbsp;';
    }
  }

  $pagign_tbl .= '</td>';

  if (isset($tdata['table']['tr']['td'][2]['a'])) {
    $pagign_tbl .= '<TD WIDTH="100" ALIGN="RIGHT" style="text-align:right;"><a href="' . thundersearch_parseURL($tdata['table']['tr']['td'][2]['a']['href']) . '"><IMG SRC="' . $urls['img_path'] . '/next.gif" HSPACE="0" VSPACE="0" BORDER="0"></a></TD>';
  }
  else {
    $pagign_tbl .= '<TD WIDTH="100" ALIGN="LEFT"></TD>';
  }
  $pagign_tbl .= '</tr></table>';
  return $pagign_tbl;
}

/**
 * Function thundersearch_parseURL().
 */
function thundersearch_parseURL($url) {
  $urls = thundersearch_getUrls();
  $url = substr($url, strpos($url, 'texis'));
  return str_replace("texis/search/main.html", $urls['result_url'], $url);
}

/**
 * Function thundersearch_getArray().
 */
function thundersearch_getArray(DOMNode $o_dom_node = NULL) {

  // Return empty array if dom is blank.
  if (is_null($o_dom_node) && !$o_dom_node->hasChildNodes()) {
    return array();
  }
  $o_dom_node = (is_null($o_dom_node)) ? $o_dom_node->documentElement : $o_dom_node;

  if (!$o_dom_node->hasChildNodes()) {
    $m_result = $o_dom_node->nodeValue;
  }
  else {
    $m_result = array();
    foreach ($o_dom_node->childNodes as $o_child_node) {
      // How many of these child nodes do we have?.
      // This will give us a clue as to what the result structure should be.
      $o_child_node_list = $o_dom_node->getElementsByTagName($o_child_node->nodeName);
      $i_child_count = 0;
      // There are x number of childs in this node that have the same tag name.
      // However, we are only interested in the # of.
      // Siblings with the same tag name.
      foreach ($o_child_node_list as $o_node) {
        if ($o_node->parentNode->isSameNode($o_child_node->parentNode)) {
          $i_child_count++;
        }
      }
      $m_value = thundersearch_getArray($o_child_node);
      $s_key = ($o_child_node->nodeName{0} == '#') ? 0 : $o_child_node->nodeName;
      $m_value = is_array($m_value) ? $m_value[$o_child_node->nodeName] : $m_value;

      // How many of thse child nodes do we have?
      // More than 1 child - make numeric array.
      if ($i_child_count > 1) {
        $m_result[$s_key][] = $m_value;
      }
      // Added by Rajnish.
      elseif ($o_child_node->nodeName == 'abstract') {
        $m_result[$s_key][0] = $o_child_node->nodeValue;
        if (isset($m_value['b']) && is_array($m_value['b'])) {
          foreach ($m_value['b'] as $rval) {
            $m_result[$s_key][0] = str_replace($rval, "<b>" . $rval . "</b>", $m_result[$s_key][0]);
          }
        }
      }
      // Added by Rajnish.
      elseif ($o_child_node->nodeName == 'resulttitle') {
        $m_result[$s_key][0] = $o_child_node->nodeValue;
        if (isset($m_value['b']) && is_array($m_value['b'])) {
          foreach ($m_value['b'] as $rval) {
            $m_result[$s_key][0] = str_replace($rval, "<b>" . $rval . "</b>", $m_result[$s_key][0]);
          }
        }
      }
      else {
        $m_result[$s_key] = $m_value;
      }

      if ($o_child_node->nodeName == '#text' && (strpos($m_result[0], 'matching documents') > 0)) {
        $m_result['#text'] = $m_result[0];
      }
    }

    // If the child is <foo>bar</foo>, the result will be array(bar).
    // Make the result just 'bar'.
    if (count($m_result) == 1 && isset($m_result[0]) && !is_array($m_result[0])) {
      $m_result = $m_result[0];
    }
  }
  // Get our attributes if we have any.
  $arattributes = array();
  if ($o_dom_node->hasAttributes()) {
    foreach ($o_dom_node->attributes as $sattrname => $oattrnode) {
      // Retain namespace prefixes.
      $arattributes["{$oattrnode->nodeName}"] = $oattrnode->nodeValue;
      // Added by Gio because of the "Unused variable $sattrname" error.
      unset($sattrname);
    }
  }

  if ($o_dom_node instanceof DOMElement && $o_dom_node->getAttribute('xmlns')) {
    $arattributes["xmlns"] = $o_dom_node->getAttribute('xmlns');
  }
  if (count($arattributes)) {
    if (!is_array($m_result)) {
      $m_result = (trim($m_result)) ? array($m_result) : array();
    }
    $m_result = array_merge($m_result, $arattributes);
  }
  $ar_result = array($o_dom_node->nodeName => $m_result);
  return $ar_result;
}
